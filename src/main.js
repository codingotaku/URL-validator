const EMPTY = "The URL field is empty!";
const FILE = "file";
const FOLDER = "folder";
const INVALID = "The URL is invalid!";
const INPUT_METHOD_EDITOR_CODE = 229;
const NOT_FOUND = "The URL is valid, but it does not exist or is unreachable!";
const PROCESSING = "Processing...";
const THROTTLE = 2000;
const TYPING = "Waiting to finish typing...";

const isFile = (path) => path.split("/").pop().indexOf(".") > 0;

let inputElement;
let outputElement;
let timers = [];
let userInput = "";

function generateResult(result) {
  return result.resourceExist
    ? `The URL is a valid ${result.resourceType}!`
    : NOT_FOUND;
}

function setResult(result) {
  if (!userInput) {
    outputElement.textContent = EMPTY;
    return;
  }

  if (result.isTyping) {
    outputElement.textContent = TYPING;
    return;
  }

  if (result.isProcessing) {
    outputElement.textContent = PROCESSING;
    return;
  }

  let outputText = result.isValid ? generateResult(result) : INVALID;
  outputElement.textContent = outputText;
}

async function validateURL() {
  //TODO: send the URL to the server so that it can download and verify the file instead
  /*
     const response = await fetch(THE_API_URL,{data: userInput});
     return await response.json();
   */

  return new Promise((resolve) => {
    setResult({ isProcessing: true });

    timers.push(
      setTimeout(() => {
        try {
          const url = new URL(userInput); // validate the URL locally once again, in case there was a race condition
          resolve({
            isValid: true,
            resourceExist: ["http:", "https:", "ftp:"].includes(url.protocol),
            resourceType: isFile(url.pathname) ? FILE : FOLDER,
          });
        } catch {
          resolve({ isValid: false });
        }
      }, 2500), // add some delay to mock requesting a server
    );
  });
}

function clearTimeouts(result) {
  setResult(result);
  timers.forEach(clearTimeout);
  timers = [];
}

function processInput({ value, isTyping, isProcessing }) {
  userInput = value;

  try {
    new URL(userInput);
    clearTimeouts({ isTyping, isProcessing });

    timers.push(
      setTimeout(async () => {
        setResult(await validateURL());
      }, THROTTLE),
    );
  } catch {
    setResult({ isValid: false });
  }
}

function isComposing(event) {
  return (
    event.isComposing ||
    event.keyCode === INPUT_METHOD_EDITOR_CODE ||
    inputElement.value === userInput
  );
}

function handleKeyUp(event) {
  const newInput = inputElement.value;

  if (isComposing(event)) {
    return;
  }

  processInput({ value: newInput, isTyping: true });
}

function handleKeyDown(event) {
  if (isComposing(event)) {
    return;
  }

  clearTimeouts({ isTyping: true });
}

function tryProcessingExistingInput() {
  const queryText =
    inputElement.value || new URL(window.location).searchParams.get("url");

  if (queryText) {
    inputElement.value = queryText;
    processInput({ value: queryText, isProcessing: true });
  }
}

function initialize() {
  outputElement = document.getElementById("output");
  inputElement = document.getElementById("url");
  tryProcessingExistingInput();

  inputElement.addEventListener("keyup", handleKeyUp);
  inputElement.addEventListener("keydown", handleKeyDown);
}

document.addEventListener("DOMContentLoaded", (event) => {
  initialize();
});
